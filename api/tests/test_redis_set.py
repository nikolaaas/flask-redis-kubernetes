import pytest 
import fakeredis
from application.redisset import RedisSet


@pytest.fixture
def redis_dummy_server():
    return fakeredis.FakeStrictRedis(decode_responses=True)

@pytest.fixture
def dummy_set():
    return {'10','20','80'}

@pytest.fixture
def redis_dummy_set_data(redis_dummy_server, dummy_set):
    redis_dummy_server.sadd('test', *dummy_set)
    return redis_dummy_server

@pytest.fixture
def redisSet_instance(redis_dummy_set_data):
    return RedisSet(redis_dummy_set_data, 'test')

def test_redisSet(redisSet_instance, dummy_set):
    test_data = {'10','70'}
    # test the intersection
    assert redisSet_instance & test_data == dummy_set & test_data
    assert test_data & redisSet_instance == test_data & dummy_set
    
    # test the union 
    assert redisSet_instance | test_data == dummy_set | test_data
    assert test_data | redisSet_instance == test_data | dummy_set

    # test difference
    assert redisSet_instance - test_data == dummy_set - test_data
    assert test_data - redisSet_instance == test_data - dummy_set


