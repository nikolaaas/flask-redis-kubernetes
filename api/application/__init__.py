from flask import Flask
from .blueprints import my_blueprint
import logging
from flask.logging import default_handler
from logging import StreamHandler

def config_logs(app: Flask) -> None:
    """
    Configures the app logging

    Parameters
    ----------
    app : Flask
        Input the flask app to be configured
    """
    app.logger.removeHandler(default_handler)
    handler = StreamHandler()
    formatter = logging.Formatter('[%(asctime)s] %(levelname)s '
                                    'in %(module)s: %(message)s') 
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)
    app.logger.setLevel(logging.INFO)


def create_app() -> Flask:
    """
    Creates a flask instance

    Returns
    -------
    Flask
        the flask app to be used
    """
    app = Flask(__name__, instance_relative_config=False)
    app.register_blueprint(my_blueprint)
    config_logs(app)
    return app