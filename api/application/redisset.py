from redis import Redis

class RedisSet:
    def __init__(self, redis_con: Redis, key: str) -> None:
        """
        Wrapper class to emulate native (python) set operations 
        using the redis server set operations

        Parameters
        ----------
        redis_con : Redis
            Redis client connection to the redis server
        key : str
            The key of the data we need to access and do
            operations on its values. 
        """
        self.redis = redis_con
        self.key = key

    def __and__(self, other: set) -> set:
        pipe = self.redis.pipeline()
        pipe.sadd('tmp', *other).sinter(self.key, 'tmp').delete('tmp')
        result = pipe.execute()        
        if result[0] == len(other) and result[2] == 1:
            return result[1]
    
    def __rand__(self, other: set) -> set:
        return self.__and__(other)

    def __or__(self, other: set) -> set:
        pipe = self.redis.pipeline()
        pipe.sadd('tmp', *other).sunion(self.key, 'tmp').delete('tmp')
        result = pipe.execute()        
        if result[0] == len(other) and result[2] == 1:
            return result[1]
    
    def __ror__(self, other: set) -> set:
        return self.__or__(other)
   
    def __sub__(self, other: set) -> set:
        pipe = self.redis.pipeline()
        pipe.sadd('tmp', *other).sdiff(self.key, 'tmp').delete('tmp')
        result = pipe.execute()        
        if result[0] == len(other) and result[2] == 1:
            return result[1] 

    def __rsub__(self, other: set) -> set:
        pipe = self.redis.pipeline()
        pipe.sadd('tmp', *other).sdiff('tmp', self.key).delete('tmp')
        result = pipe.execute()        
        if result[0] == len(other) and result[2] == 1:
            return result[1]