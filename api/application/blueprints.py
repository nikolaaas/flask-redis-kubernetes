from flask import Blueprint, jsonify, request
from flask import current_app
from .redisset import RedisSet
from .redis_conf import my_redis, MY_SET_KEY

my_blueprint = Blueprint('my_blueprint', __name__)

mydata = RedisSet(my_redis, MY_SET_KEY)

@my_blueprint.route('/')
def hello():
    current_app.logger.info("hello world")
    return jsonify({'hello': 'world'})

@my_blueprint.route('/union', methods=['GET'])
def set_union():
    """ 
    simple demo GET request of sending a set of strings
    and returning the union of the input and the strings
    stored on the redis server 
    """ 
    input_set = set(request.args.get('input_set','').split(','))
    return jsonify({'union': list(input_set & mydata)})