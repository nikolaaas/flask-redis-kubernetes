from redis import Redis

MY_SET_KEY = 'set_key1'

my_redis = Redis(host='localhost',
                port=6379,
                charset='utf-8',
                decode_responses=True,
                socket_keepalive=True)
