from redis import Redis
from time import sleep
from application import create_app
from application.redis_conf import my_redis, MY_SET_KEY

app = create_app()

while True:
    try:
        my_redis.ping()
        if my_redis.exists(MY_SET_KEY):
            app.logger.info('Connection and key in place')
            break
        else:
            app.logger.info('Connection exists but key is NOT in place')
            sleep(3) 
    except Exception as e:
        app.logger.info('Failed attempt to connect with redis')
        sleep(3)
        continue

app.logger.info('I have started')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)