# Flask API with redis cache (sidecar) on Kubernetes 

An API demo using flask and redis (as a sidecar container) to query using a scheduler (as a sidecar container as well) and cache large datasets in memory independent of  concurent workers from uwsgi serving the application. With this configuration we make sure that there is no blocking of service during a long scheduled update of the data. Moreover, the uwsi server is using pre-forking, spawning multiple processes which do not share memory thus the redis cache serves as "shared" memory. On top of that redis already is oprimized and provides perations on sets and lists, so instead of copying the data and operating on the application side we can wrap a class of these types and operate on redis side improving performance. Finaly, by using the sidecar design pattern in kubernetes we make sure that the redis instance will be as close as possible to the application instance with respect to hardware nodes thus reducing latency and improving performance. The only tradeoff for this performance oriented setup is that as we scale up the pod replicas we are using more RAM memory as the data for each pod is the same and we end up with multiple instances carrying the same data. 

The following schematic presents the container/pod layout
![png](pod_schematic.png)
 
 The following schematic presents the kubernetes cluster/pod layout
![png](kubernetes_schematic.png)

## Requirements

In order to experiment with the API you need to install k3d, so you can emulate a kubernetes cluser (in case a real one is not available). Then execute the following script to setup the cluster I used for testing/development

```bash
./create_k8s_cluster.sh
```

## Tesing

To run the API, after you have setup the k3d cluster, you need to setup an image repository (I use gitlabs), in order to be able to pull the images of your builds (in this case the api and scheduler images). When you have this setup, a kubernetes secret is required to be created (in the deployment directory) so you can actually pull the images from your repository using your credentials in a safe way. Finally, you need to modify the file "deployments/deployment.yml" with your secret and image repository urls and run the following command to deploy it:

```bash
 kubectl apply -f deployment/
```

and you can test that the API works with the following command:

```bash
curl -X GET "http://localhost:8081/union?input_set=1,2,80000000"
```