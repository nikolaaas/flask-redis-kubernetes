import os
from typing import List
from redis import Redis
import logging
from apscheduler.schedulers.blocking import BlockingScheduler
from typing import List

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',
                    level=os.getenv("LOG_LEVEL"))

MY_SET_KEY = 'set_key1'

my_redis = Redis(host='localhost',
                      port=6379,
                      charset='utf-8',
                      decode_responses=True,
                      socket_keepalive=True)

def chunks(input_lst: List, chunk_size: int):
    """
    Yields chuncks of a list of size chunk_size
    The last chunk can vary in size depending 
    the len(input_lst)/chunck_size 

    Parameters
    ----------
    input_lst : List
        The input list to be chunked
    chunk_size : int
        Size of the returned chuncks        

    Yields
    -------
    Iterator[List]
        The chunk of the list
    """
    for i in range(0, len(input_lst), chunk_size):
        yield input_lst[i:i+chunk_size]

def update_fun():
    # here you can query data from a DB but for this demo 
    # we create a big list of integers.
    dummy_data = [i for i in range(20000000)]
    
    tmp_key = MY_SET_KEY + '_tmp'
    chunk_size = 50000

    try:
        logging.info('starting update')
        for ind, ch in enumerate(chunks(dummy_data, chunk_size)):
            my_redis.sadd(tmp_key, *ch)
            logging.info('chunk-' + str(ind)
                        + ' memory used '
                        + my_redis.info()['used_memory_human'])
        my_redis.rename(tmp_key, MY_SET_KEY)
        logging.info('Update finised')
    except Exception as e:
        logging.warn('Failer to update')

if __name__ == '__main__':
    while True:
        try:
            if my_redis.ping():
                logging.warn('Connection with redis in place')
                break
        except Exception as e:
            logging.warn('Failed attempt to connect with redis')
            continue
    
    scheduler = BlockingScheduler()
    update_fun()
    scheduler.add_job(update_fun, 'cron', hour='0,12', minute='0')
    # scheduler.add_job(update_fun, 'interval', minutes=6)
    scheduler.start()
