#!/bin/bash

# create a local containerized kubernetes cluster with 3 server nodes,
# 3 agent nodes and the internal port 80 
# (where the traefik ingress controller is listening on) is exposed 
# on the host system (localhost).

# the port-mapping construct 8081:80@loadbalancer means:
# “map port 8081 from the host to port 80 on the container which matches the nodefilter loadbalancer“

# the loadbalancer nodefilter matches only the serverlb that’s deployed in front of a cluster’s server nodes
# all ports exposed on the serverlb will be proxied to the same ports on all server nodes in the cluster

k3d cluster create my-test-cluster --servers 3 --agents 3 -p "8081:80@loadbalancer"
